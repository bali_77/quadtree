var files =
[
    [ "compress.c", "compress_8c.html", "compress_8c" ],
    [ "compress.h", "compress_8h.html", "compress_8h" ],
    [ "debug.c", "debug_8c.html", "debug_8c" ],
    [ "debug.h", "debug_8h.html", "debug_8h" ],
    [ "eventMlv.c", "event_mlv_8c.html", "event_mlv_8c" ],
    [ "eventMlv.h", "event_mlv_8h.html", "event_mlv_8h" ],
    [ "main.c", "main_8c.html", "main_8c" ],
    [ "openImg.c", "open_img_8c.html", "open_img_8c" ],
    [ "openImg.h", "open_img_8h.html", "open_img_8h" ],
    [ "optionLoad.c", "option_load_8c.html", "option_load_8c" ],
    [ "optionLoad.h", "option_load_8h.html", "option_load_8h" ],
    [ "pixel.h", "pixel_8h.html", [
      [ "pixel", "structpixel.html", "structpixel" ]
    ] ],
    [ "quadTree.c", "quad_tree_8c.html", "quad_tree_8c" ],
    [ "quadTree.h", "quad_tree_8h.html", "quad_tree_8h" ],
    [ "rgbaTool.c", "rgba_tool_8c.html", "rgba_tool_8c" ],
    [ "rgbaTool.h", "rgba_tool_8h.html", "rgba_tool_8h" ],
    [ "save.c", "save_8c.html", "save_8c" ],
    [ "save.h", "save_8h.html", "save_8h" ],
    [ "shapes.c", "shapes_8c.html", "shapes_8c" ],
    [ "shapes.h", "shapes_8h.html", "shapes_8h" ],
    [ "treeMlv.c", "tree_mlv_8c.html", "tree_mlv_8c" ],
    [ "treeMlv.h", "tree_mlv_8h.html", "tree_mlv_8h" ],
    [ "windowMlv.c", "window_mlv_8c.html", "window_mlv_8c" ],
    [ "windowMlv.h", "window_mlv_8h.html", "window_mlv_8h" ]
];