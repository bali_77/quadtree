var searchData=
[
  ['closeimage',['closeImage',['../open_img_8h.html#a3ddab17986dfb7c31a41bd1655090f50',1,'closeImage(myImage *img):&#160;openImg.c'],['../open_img_8c.html#a3ddab17986dfb7c31a41bd1655090f50',1,'closeImage(myImage *img):&#160;openImg.c']]],
  ['closewindow',['closeWindow',['../window_mlv_8h.html#a6fcb05de762da60f52d18d665c3b67d8',1,'closeWindow():&#160;windowMlv.c'],['../window_mlv_8c.html#a6fcb05de762da60f52d18d665c3b67d8',1,'closeWindow():&#160;windowMlv.c']]],
  ['color',['color',['../structnoeud.html#aed2306006f8f164b13626ca6d957a4b8',1,'noeud']]],
  ['comp',['comp',['../compress_8h.html#a6a35b2f3dac3f87345f5edcb34099bbd',1,'comp(QuadTree *un, QuadTree *deux, int seuil):&#160;compress.c'],['../compress_8c.html#a6a35b2f3dac3f87345f5edcb34099bbd',1,'comp(QuadTree *un, QuadTree *deux, int seuil):&#160;compress.c']]],
  ['compress',['compress',['../compress_8h.html#a94c432dd8f61e2e370ff97a7ed94fcbb',1,'compress(QuadTree *root, int seuil):&#160;compress.c'],['../compress_8c.html#a94c432dd8f61e2e370ff97a7ed94fcbb',1,'compress(QuadTree *root, int seuil):&#160;compress.c']]],
  ['compress_2ec',['compress.c',['../compress_8c.html',1,'']]],
  ['compress_2eh',['compress.h',['../compress_8h.html',1,'']]],
  ['compressleaf',['compressLeaf',['../compress_8h.html#a33befb9230d6112bf62f05a1a8ccbc0d',1,'compressLeaf(QuadTree *root):&#160;compress.c'],['../compress_8c.html#a33befb9230d6112bf62f05a1a8ccbc0d',1,'compressLeaf(QuadTree *root):&#160;compress.c']]],
  ['convertbittoint',['convertBitToInt',['../quad_tree_8h.html#a8928855e5b6fe9b7a1a8c85e573f80d2',1,'convertBitToInt(int *tab):&#160;quadTree.c'],['../quad_tree_8c.html#a8928855e5b6fe9b7a1a8c85e573f80d2',1,'convertBitToInt(int *tab):&#160;quadTree.c']]]
];
