var searchData=
[
  ['main',['main',['../main_8c.html#ae66f6b31b5ad750f1fe042a706a4e3d4',1,'main.c']]],
  ['messageuser',['messageUser',['../window_mlv_8h.html#a4ed8564080b7e9b35a860ef819e7ef91',1,'messageUser(char *s):&#160;windowMlv.c'],['../window_mlv_8c.html#a4ed8564080b7e9b35a860ef819e7ef91',1,'messageUser(char *s):&#160;windowMlv.c']]],
  ['moyenne',['moyenne',['../rgba_tool_8h.html#abe8c4a4972b587b1eabf109d54c6f34d',1,'moyenne(rgba **tab, int width, int height):&#160;rgbaTool.c'],['../rgba_tool_8c.html#abe8c4a4972b587b1eabf109d54c6f34d',1,'moyenne(rgba **tab, int width, int height):&#160;rgbaTool.c']]],
  ['moyennezone',['moyenneZone',['../rgba_tool_8h.html#a4d1be9ddda14dc49aae85204cc81a977',1,'moyenneZone(myImage *img, pixel a, pixel b):&#160;rgbaTool.c'],['../rgba_tool_8c.html#a4d1be9ddda14dc49aae85204cc81a977',1,'moyenneZone(myImage *img, pixel a, pixel b):&#160;rgbaTool.c']]],
  ['moyennezonergba',['moyenneZoneRgba',['../rgba_tool_8h.html#a97d9855435f315306c313fcd79f9b852',1,'moyenneZoneRgba(myImage *img, pixel a, pixel bn, rgba *color):&#160;rgbaTool.c'],['../rgba_tool_8c.html#addeab47b986a560f526b04a558c0ee14',1,'moyenneZoneRgba(myImage *img, pixel a, pixel b, rgba *color):&#160;rgbaTool.c']]],
  ['moyennezonergba2',['moyenneZoneRgba2',['../rgba_tool_8h.html#a2bdc06897fe72108eed423bfa57c6b9f',1,'moyenneZoneRgba2(myImage *img, pixel a, pixel b, rgba *color):&#160;rgbaTool.c'],['../rgba_tool_8c.html#a2bdc06897fe72108eed423bfa57c6b9f',1,'moyenneZoneRgba2(myImage *img, pixel a, pixel b, rgba *color):&#160;rgbaTool.c']]]
];
