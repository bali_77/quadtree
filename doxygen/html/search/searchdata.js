var indexSectionsWithContent =
{
  0: "abcdefgilmnopqrstwxy",
  1: "mnpr",
  2: "cdemopqrstw",
  3: "abcdefgilmpstw",
  4: "abcefgilmnoprstxy",
  5: "nq",
  6: "dlmnpqrs"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Structures de données",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables",
  5: "Définitions de type",
  6: "Macros"
};

