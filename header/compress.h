#ifndef COMPRESS_H_INCLUDED
#define COMPRESS_H_INCLUDED

#include "quadTree.h"


/** \brief calcule la distance couleur entre deux branches
 *
 * \param one QuadTree branche un
 * \param two QuadTree branche deux
 * \return double resultat
 *
 */
double distanceTree(QuadTree one , QuadTree two);

/** \brief compresse le quadTree en testant toutes les possibilites
 *
 * \param root QuadTree* le quadTree
 * \param seuil int notre seuil
 * \return void
 *
 */
void compress(QuadTree *root,int seuil);

/** \brief compresse si le seuil est bon , supprime le deuxieme et le raccorde au premier
 *
 * \param un QuadTree* à garder
 * \param deux QuadTree* à supprimer et remplacer par un
 * \param seuil int le seuil
 * \return int si tout c'est bien passé
 *
 */
int comp(QuadTree *un , QuadTree *deux, int seuil);

/** \brief comprime les feuilles de meme couleur
 *
 * \param root QuadTree*
 * \return void
 *
 */
void compressLeaf(QuadTree *root);

#endif
