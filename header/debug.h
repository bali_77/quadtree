#ifndef DEBUG_H_INCLUDED
#define DEBUG_H_INCLUDED


#define DEBUG 0  /**< activé ou désativé */
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

/** \brief Affiche les messages deub sur l'entrée standart
 *
 * \param nbrVar int nombre de messages
 * \param ... les messages
 * \return void rien
 *
 */
void debugMessage(int nbrVar,...);

#endif
