#ifndef EVENTMLV_H_INCLUDED
#define EVENTMLV_H_INCLUDED


#include <MLV/MLV_all.h>
#include "debug.h"

typedef struct{
    int x; /**< pos x */
    int y; /**< pos y */
    int pressed;/**<  pressé ou pas */
    int oldevent;/**<  l'ancien evenement */

}myEvent;




/** \brief on recupere un evenement grace à la lib mlv
 *
 * \param e myEvent* notre evenement
 * \return void
 *
 */
void events(myEvent *e);

#endif
