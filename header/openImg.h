#ifndef OPENIMG_H_INCLUDED
#define OPENIMG_H_INCLUDED

#include <MLV/MLV_all.h>

#include "debug.h"
#include "windowMlv.h"

#define DEFAULTERROR 200

typedef struct{
    MLV_Image *img;/**< une image exterieure */
    char* fimg; /**< image perso */
    char ext[3]; /**< extension qtc ou gmc */
    int TAUXERREUR; /**< TAUX ERREUR de base du traitement de l'image */
}myImage;


/** \brief Demande à l'utilisateur de charger une image , ici les formats pris en charge sont les formats exterieur géré par la
lib MLV et les perso aux projets gmc/qtc
 *
 * \param type int le type d'image 0 / exterieur 1/ gmc qtc
 * \return myImage Return une structure contenant l'image chargé
 *
 */
myImage loadImage(int type);

/** \brief On referme toutes les images ouvertes dans la structure
 *
 * \param img myImage* la structure myImage
 * \return void Return void
 *
 */
void closeImage(myImage *img);


/** \brief parseur pour les extensions qtc et gmc
 *
 * \param name char* name le nom de l'image + son extension ( img.extension par exemple )
 * \param endd char* endd l'exentension exemple gmc ...
 * \return int 0 ou 1 , 0 erreur 1 ok
 *
 */
int parser(char *name,char *endd);

#endif
