#ifndef OPTIONLOAD_H_INCLUDED
#define OPTIONLOAD_H_INCLUDED

#include <MLV/MLV_all.h>

#include "windowMlv.h"
#include "openImg.h"
#include "quadTree.h"
#include "save.h"


/** \brief on traute les options si elles sont valides
 *
 * \param opt int option à traiter
 * \param img myImage* l'image
 * \param vit int* vitesse/erreur
 * \param t QuadTree le quadtree
 * \return void void
 *
 */
void loadOpt(int opt,myImage * img, int *vit,QuadTree t);

#endif
