#ifndef QUADTREE_H_INCLUDED
#define QUADTREE_H_INCLUDED

#include <string.h>
#include <MLV/MLV_all.h>

#include "rgbaTool.h"
#include "openImg.h"
#include "pixel.h"
#include "shapes.h"



/** \brief une structure de notre quadtree
 */
typedef struct noeud{
    rgba color; /**< la couleur */
    MLV_Color mColor;/**< la couleur mlv */
    int freeKey ;/**<  si on libere ou pas*/
    double erreur;/**< taux erreur */
    struct noeud * NO;/**<  noeud no*/
    struct noeud * NE;/**<  noeud ne*/
    struct noeud * SO;/**<  noeud so*/
    struct noeud * SE;/**<  noeud se*/
    int lenght; /**< connaitre sur quel noeud on est , 1 NO 2 NE 3 SO 4 SE */

}Noeud,*QuadTree;



/** \brief on traite dej pour savoir quel fichier on a img exterieure ou bien gmc/qtc
 *
 * \param img myImage* notre image à traiter
 * \param vitesse int le taux d'erreur
 * \param a pixel un pixel base 0 0
 * \param b pixel un pixel base MAX X MAX Y
 * \param root QuadTree* notre quadtree
 * \return void
 *
 */
void postTraitement(myImage *img, int vitesse, pixel a, pixel b , QuadTree *root);


/** \brief on traite une image exterieur
 *
 * \param img myImage* l'image dans la lib mlv
 * \param vitesse int la vitesse de traitement/ erreur
 * \param a pixel pixel 0 0
 * \param b pixel pixel MAX X MAX Y
 * \param root QuadTree* notre quadTRee ou on va enregistrer
 * \return void
 *
 */
void traitement(myImage *img, int vitesse , pixel a, pixel b, QuadTree *root);



/** \brief traite une image qtc
 *
 * \param img myImage* l'image
 * \param vitesse int vitesse/tauxerreur
 * \param root QuadTree* le quadTree ou on va l'enregistrer
 * \return void
 *
 */
void traitementQtc(FILE *img, int vitesse, QuadTree * root );



/** \brief on traite un gmc
 *
 * \param img myImage* l'iamge concernee
 * \param vitesse int la vitesse/taux erreur
 * \param root QuadTree* la sauvegarde du traitement
 * \param oldColor rgba* l'ancienne couleur utilisé
 * \return void
 *
 */
void traitementGmc(FILE *img, int vitesse, QuadTree * root, rgba *oldColor);



/** \brief convertie un tableau de 8 bit en int
 *
 * \param tab int* tableau de int
 * \return int un int
 *
 */
int convertBitToInt(int *tab);



/** \brief renvoie un quadtree vide
 *
 * \return QuadTree
 *
 */
QuadTree initQuadTree();

/** \brief vide un quadtree
 *
 * \param tmp QuadTree* le quadTree
 * \return void
 *
 */
void freeQuadTree(QuadTree *tmp);

/** \brief vide un quadtree compréssé
 *
 * \param tmp QuadTree*
 * \return void
 *
 */
void freeQuadTreeCompress(QuadTree *tmp);


/** \brief renvoie si oui ou non c'est une feuille
 *
 * \param tmp QuadTree*
 * \return int 0 non  ou 1 oui
 *
 */
int isLeaf(QuadTree *tmp);

/** \brief renvoie un noeud vide avec une couleur predefinie et sa pos dans un carre
 *
 * \param rgba rgba la couleur
 * \param i int sa position 1 , 2 , 3 ou 4
 * \return QuadTree
 *
 */
QuadTree getEmptyNode(rgba rgba, int i); /* get empty nodde with the color */

/** \brief divise un rectangle en 4 puis renvoie les coordonnes haut gauche dans a et bas droite dans b de la position souhaite
 *
 * \param number int 1 NO, 2NE, 3SO ou 4SE
 * \param a pixel* haut gauche
 * \param b pixel* bas droite
 * \return void
 *
 */
void getRectangle(int number, pixel *a, pixel *b);

/** \brief renvoie l'air d'un rectangle
 *
 * \param a pixel
 * \param b pixel
 * \return int aire du rectangle
 *
 */
int areaRectangle(pixel a , pixel b);

#endif
