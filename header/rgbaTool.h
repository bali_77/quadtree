#ifndef RGBATOOL_H_INCLUDED
#define RGBATOOL_H_INCLUDED

#include <math.h>
#include "debug.h"
#include "pixel.h"
#include <MLV/MLV_all.h>
#include "openImg.h"

/**< une structure rgba */
typedef struct{
    int red;
    int green;
    int blue;
    int alpha;
}rgba;


/** \brief distance entre deux couleurs
 *
 * \param un rgba color un
 * \param deux rgba color deux
 * \return double resultat
 *
 */
double distance(rgba un ,rgba deux);

/** \brief renvoie la moyenne des couleurs rgba du tableau
 *
 * \param tab rgba** le tableau rgba
 * \param width int sa taille x
 * \param height int sa taille y
 * \return rgba une couleur moyenne
 *
 */
rgba moyenne(rgba **tab,int width, int height);

/** \brief renvoie le taux d'erreur entre la couleur moyenne et la couleur reelle
 *
 * \param img myImage* l'image
 * \param a pixel zone haut gauche
 * \param b pixel zone bas droite
 * \param z rgba la couleur moyenne
 * \return double resulat
 *
 */
double erreurZ(myImage *img,pixel a, pixel b, rgba z);

/** \brief renvoie moyenne d'une zone de l'image en mlv
 *
 * \param img myImage* l'image
 * \param a pixel haut gauche
 * \param b pixel bas droite
 * \return MLV_Color la couleur en mlv
 *
 */
MLV_Color moyenneZone(myImage *img, pixel a , pixel b );

/** \brief anciene version voir moyenneZoneRgba2
 *
 * \param img myImage*
 * \param a pixel
 * \param bn pixel
 * \param color rgba*
 * \return double
 *
 */
double moyenneZoneRgba(myImage *img, pixel a, pixel bn, rgba *color);

/** \brief moyenne zone d'une image avec taux erreur calculé en temps réel
 *
 * \param img myImage* l'image
 * \param a pixel haut gauche
 * \param b pixel bas droite
 * \param color rgba* couleur moyenne
 * \return double taux erreur
 *
 */
double moyenneZoneRgba2(myImage * img, pixel a, pixel b, rgba * color);

/** \brief renvoie un tableau 2d rgba initialisé
 *
 * \param width int width
 * \param height int height
 * \return rgba**
 *
 */
rgba ** getRgbaTab(int width, int height);


/** \brief on libere un tableau 2d rgba
 *
 * \param tab rgba**
 * \param width int
 * \return void
 *
 */
void freeTab2d(rgba **tab, int width);

#endif
