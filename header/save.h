#ifndef SAVE_H_INCLUDED
#define SAVE_H_INCLUDED

#include "quadTree.h"


/** \brief sauvegarde un quadtree non minimisé
 *
 * \param root QuadTree le quadtree
 * \return void
 *
 */
void saveQuadTree(QuadTree root);


/** \brief sauvegarde un quadtree minimisé
 *
 * \param root QuadTree le quadtree
 * \return void
 *
 */
void saveQuadTreeM(QuadTree root);

/** \brief on verifie que loe nom donné par l'utilisateur est bon
 *
 * \param name char* nom
 * \param endd char* extension à verifier
 * \return int
 *
 */
int parserSave(char * name, char * endd);

/** \brief ecrit dans file en mode binaire , compresse ou non
 *
 * \param file FILE* ou on sauvegarde
 * \param root QuadTree ce que l'on sauvegarde
 * \return void
 *
 */
void writeTreeBinary(FILE* file, QuadTree root);

/** \brief voir writeTreeCompress2
 *
 * \param file FILE*
 * \param root QuadTree
 * \return void
 *
 */
void writeTreeCompress(FILE * file ,QuadTree root);


/** \brief ecrit dans file le quadtree compresse/non compresse en mode ASCII
 *
 * \param file FILE* le fichier de sauvegare
 * \param root QuadTree le fichier
 * \param color int
 * \return void
 *
 */
void writeTreeCompress2(FILE * file ,QuadTree root, int color);

/** \brief ecrit une couleur en controlant que c'est pas un doublon
 *
 * \param file FILE* le fichier
 * \param root QuadTree le quadtree
 * \param carre int la position du carre
 * \return void
 *
 */
void writeColor(FILE *file, QuadTree root, int carre);

#endif
