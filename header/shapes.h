#ifndef SHAPES_H_INCLUDED
#define SHAPES_H_INCLUDED

#include <MLV/MLV_all.h>
#include "eventMlv.h"
#include "pixel.h"
#include "rgbaTool.h"
#include "windowMlv.h"

/** \brief dessine un rectangle plein grace à la couleur rgba sur la fenetre mlv
 *
 * \param a pixel haut gauche
 * \param b pixel bas droite
 * \param color rgba couleur
 * \return void
 *
 */
void drawFilledRectangle(pixel a , pixel b , rgba color);

/** \brief dessine un rectangle plein grace à la couleur mlv sur la fenetre mlv
 *
 * \param a pixel haut gauche
 * \param b pixel bas droite
 * \param color MLV_Color couleur
 * \return void
 *
 */
void drawFilledRectangleM(pixel a, pixel b, MLV_Color color);

#endif
