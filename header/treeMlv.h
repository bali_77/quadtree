#ifndef TREEMLV_H_INCLUDED
#define TREEMLV_H_INCLUDED

#include "quadTree.h"
#include "windowMlv.h"

/** \brief dessine le quadTree sur l'entree mlv
 *
 * \param t QuadTree* le quadtree à dessiner
 * \param a pixel 0,0
 * \param b pixel MAXS,MAXY
 * \return void
 *
 */
void printTree(QuadTree * t, pixel a, pixel b);


#endif
