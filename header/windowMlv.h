#ifndef WINDOWMLV_H_INCLUDED
#define WINDOWMLV_H_INCLUDED


#include <MLV/MLV_all.h>

#include "debug.h"
#include "eventMlv.h"

#define RESX 512 /**<taille ecran et image  */
#define RESYM 528 /**< taille ecran */
#define RESY 512 /**< taille image */
#define SIZEM RESYM-RESY /**< taille du menu */
#define SIZEI RESX/10 /**< taille d'un lien du menu 10 % de la barre */

#define NBRMENU 8 /**< nombre de menu */

/**< ici les id menus */
#define LOAD 0 /**< charger ubne image exterieure */
#define LOADU 1 /**< charger un qtc ou gmc */
#define SAVEB 2 /**< sauvegarde binaire */
#define SAVEM 3 /**< sauvegarde gmc qtc */
#define MESSAGE 4 /**< barre des messages */
#define MOINS 7 /**< augmente vitesse/erreur */
#define PLUS 8  /**< baisse la vitesse/erreur */
#define START 6 /**< traitement de l'image */
#define QUIT 9 /**< on ferme */

/**< Nom des menus  */
#define LOADS "LOADI|"
#define LOADUS "LOADF|"
#define SAVEBS "SAVEB|"
#define SAVEMS "SAVEM| "
#define QUITS "|QUIT"
#define PLUSS "|-|ERR"
#define MOINSS " |+|ERR"
#define STARTS "|START"



/** \brief lance une fenetre mlv
 *
 * \return void
 *
 */
void launchWindow();


/** \brief ferme la fentre mlv
 *
 * \return void
 *
 */
void closeWindow();


/** \brief on affiche la barre des menus
 *
 * \return void
 *
 */
void barMenu();



/** \brief envoie un message à l'utilisateur sur la bersion graphique
 *
 * \param s char* le message
 * \return void
 *
 */
void messageUser(char *s);



/** \brief Demande une chaine de caractere a l'utilisateur
 *
 * \param s char** le message
 * \return void
 *
 */
void askUser(char **s);


/** \brief on traite les evenements en renvoyant le code de l'option sinon -2
 *
 * \param e myEvent* notre evenement
 * \return int l'option
 *
 */
int loadEvent(myEvent *e);
#endif
