#makefile project

GXX = gcc
myFlag = -Wall -g -Wall -ansi -pg -g -lMLV -lm


project: compress.o debug.o eventMlv.o main.o openImg.o optionLoad.o quadTree.o rgbaTool.o save.o shapes.o treeMlv.o windowMlv.o
	${GXX}  -o project obj/compress.o obj/debug.o obj/eventMlv.o obj/main.o obj/openImg.o obj/optionLoad.o obj/quadTree.o obj/rgbaTool.o obj/save.o obj/shapes.o obj/treeMlv.o obj/windowMlv.o  -pg -lMLV -lm
compress.o:
	${GXX} ${myFlag}  -c source/compress.c -o obj/compress.o
debug.o:
	${GXX} ${myFlag}  -c source/debug.c -o obj/debug.o
eventMlv.o:
	${GXX} ${myFlag}  -c source/eventMlv.c -o obj/eventMlv.o
main.o:
	${GXX} ${myFlag}  -c source/main.c -o obj/main.o
openImg.o:
	${GXX} ${myFlag}  -c source/openImg.c -o obj/openImg.o
optionLoad.o:
	${GXX} ${myFlag}  -c source/optionLoad.c -o obj/optionLoad.o
quadTree.o:
	${GXX} ${myFlag}  -c source/quadTree.c -o obj/quadTree.o
rgbaTool.o:
	${GXX} ${myFlag}  -c source/rgbaTool.c -o obj/rgbaTool.o
save.o:
	${GXX} ${myFlag}  -c source/save.c -o obj/save.o
shapes.o:
	${GXX} ${myFlag}  -c source/shapes.c -o obj/shapes.o
treeMlv.o:
	${GXX} ${myFlag}  -c source/treeMlv.c -o obj/treeMlv.o
windowMlv.o:
	${GXX} ${myFlag}  -c source/windowMlv.c -o obj/windowMlv.o
