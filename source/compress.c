#include "../header/compress.h"

/** \brief calcule la distance couleur entre deux branches
 *
 * \param one QuadTree branche un
 * \param two QuadTree branche deux
 * \return double resultat
 *
 */
double distanceTree(QuadTree one , QuadTree two){

    /* 4 cas*/
    if( isLeaf(&one) == 1 && isLeaf(&two) == 1){
        return distance(one->color , two->color);
    }
    else if( isLeaf(&one) == 1 && isLeaf(&two) == 0){
        return (distance(one->color,two->NO->color)/4) +
        (distance(one->color,two->NE->color)/4) +
        (distance(one->color,two->SO->color)/4) +
        (distance(one->color,two->SE->color)/4);
    }
    else if( isLeaf(&one) == 0 && isLeaf(&two) == 1){
        return (distance(one->NO->color,two->color)/4) +
        (distance(one->NE->color,two->color)/4) +
        (distance(one->SO->color,two->color)/4) +
        (distance(one->SE->color,two->color)/4);
    }
    else{
        return (distance(one->NO->color,two->NO->color)/4) +
        (distance(one->NE->color,two->NE->color)/4) +
        (distance(one->SO->color,two->SO->color)/4) +
        (distance(one->SE->color,two->SE->color)/4);
    }

}

/** \brief comprime les feuilles de meme couleur
 *
 * \param root QuadTree*
 * \return void
 *
 */
void compressLeaf(QuadTree *root){

        if( (*root)-> NO == NULL && (*root)->NE == NULL && (*root)->SE == NULL && (*root)->SO == NULL){
            return;
        }

    if( isLeaf(&(*root)->NO) == 1 && isLeaf(&(*root)->NE) == 1 && isLeaf(&(*root)->SO) == 1 && isLeaf(&(*root)->SE) == 1){
            /* on verifie si les feuilles ont la même couleur et la on réduit */
            if( (*root)->NO->mColor == (*root)->NE->mColor && (*root)->NE->mColor == (*root)->SO->mColor && (*root)->SO->mColor == (*root)->SE->mColor ){
                /* on libère le reste sauf NO */
                free(&(*root)->NE);
                (*root)->NE = NULL;
                free(&(*root)->SE);
                (*root)->SE = NULL;
                free(&(*root)->SO);
                (*root)->SO = NULL;
            }
            return;

    }

    compressLeaf(&(*root)->NO);
        compressLeaf(&(*root)->NE);
            compressLeaf(&(*root)->SO);
                compressLeaf(&(*root)->SE);

}

/** \brief compresse le quadTree en testant toutes les possibilites
 *
 * \param root QuadTree* le quadTree
 * \param seuil int notre seuil
 * \return void
 *
 */
void compress(QuadTree *root, int seuil){

        if( (*root) == NULL || isLeaf(&*root) == 1){
            return;
        }



        if( comp(&(*root)->NO,&(*root)->NE,seuil) == 1){
            return;
        }
        if( comp(&(*root)->NO,&(*root)->SO,seuil) == 1){
            return;
        }
        if( comp(&(*root)->NO,&(*root)->SE,seuil) == 1){
            return;
        }


        if( comp(&(*root)->NE,&(*root)->SO, seuil) == 1){
            return;
        }
        if( comp(&(*root)->NE,&(*root)->SE,seuil) == 1){
            return;
        }



        if( comp(&(*root)->SO,&(*root)->NO,seuil) == 1){
            return;
        }
        if( comp(&(*root)->SO,&(*root)->SE,seuil) == 1){
            return;
        }





            compress(&(*root)->NO,seuil);
            compress(&(*root)->NE,seuil);
            compress(&(*root)->SO,seuil);
            compress(&(*root)->SE,seuil);



}

/** \brief compresse si le seuil est bon , supprime le deuxieme et le raccorde au premier
 *
 * \param un QuadTree* à garder
 * \param deux QuadTree* à supprimer et remplacer par un
 * \param seuil int le seuil
 * \return int si tout c'est bien passé
 *
 */
int comp(QuadTree *un , QuadTree *deux, int seuil){

        if( *un != NULL && *deux != NULL && distanceTree(*un,*deux) < seuil){
            /*printf("%f \n",distanceTree((*root)->NO,(*root)->NE));*/
            freeQuadTree(&*deux);
            *deux = *un;
            return 1;
        }
    return 0;
}
