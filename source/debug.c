#include "../header/debug.h"

/** \brief Affiche les messages deub sur l'entrée standart
 *
 * \param nbrVar int nombre de messages
 * \param ... les messages
 * \return void rien
 *
 */
void debugMessage(int nbrVar,...){


    if( DEBUG == 1){
    va_list ap;
    int i = 0;

    va_start(ap, nbrVar);

    for(i=0; i < nbrVar ; i++){
        fprintf(stdout,va_arg(ap,char *));
        fprintf(stdout,"\n");
    }

    va_end(ap);
    }

}
