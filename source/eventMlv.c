#include "../header/eventMlv.h"



/** \brief on recupere un evenement grace à la lib mlv
 *
 * \param e myEvent* notre evenement
 * \return void
 *
 */
void events(myEvent *e){

   /* debugMessage(1,"On recupere un evenement");*/
        MLV_Button_state state;


        MLV_Event event = MLV_NONE;
        int x =0 , y =0;

        event = MLV_get_event(NULL,NULL,NULL,NULL,NULL,&x,&y,NULL,&state);

        /* traite les evenements */
        switch(event){
            case MLV_MOUSE_BUTTON:
                (*e).pressed = 1;
                /* on a cliqué */
                break;
            case MLV_MOUSE_MOTION:
                /* on a bougé la souris */
                (*e).x = x;
                (*e).y = y;
                break;
            default:
                return;
        }

}
