#include <stdio.h>
#include <stdlib.h>
#include <MLV/MLV_all.h>

#include "../header/windowMlv.h"
#include "../header/eventMlv.h"
#include "../header/optionLoad.h"
#include "../header/quadTree.h"
#include "../header/treeMlv.h"
#include "../header/compress.h"

int main()
{
    int quit = 0;


    myImage img = { NULL,NULL,"AAA",DEFAULTERROR};
    pixel a = {0,0};
    pixel b = {RESX,RESY};
    QuadTree tmp = NULL;
    tmp = initQuadTree();

    launchWindow();
     myEvent e = { 0,};
    do{
        events(&e);
        quit = loadEvent(&e);
        if( quit >= 0 && quit <= 10){
            loadOpt(quit,&img,&img.TAUXERREUR,tmp);
            MLV_flush_event_queue();
        }
        if(quit == 6 ){
            messageUser("Creation de l'arbre");
            postTraitement(&img,img.TAUXERREUR,a,b, &tmp);
            messageUser("Affichage de l'arbre");
            MLV_draw_filled_rectangle(0,SIZEM,RESX,RESY,MLV_COLOR_BLACK);
            printTree(&tmp,a,b);

            messageUser("Minimise ? ( -1  NO >  -1 YES)");
            MLV_wait_seconds(2);
            char *msg;
            askUser(&msg);
            int seuil = atoi(msg);
            free(msg);

            if( seuil >= 0 ){
            compress(&tmp, seuil);
            printf("Compress ok \n");
            messageUser("Traitement fini ");
                        MLV_draw_filled_rectangle(0,SIZEM,RESX,RESY,MLV_COLOR_BLACK);
                        printTree(&tmp,a,b);
            }
            MLV_flush_event_queue();
        }

    }while(quit != 9);


    /* on nettoie */
    messageUser("Vide QuadTree 0/100");
    MLV_wait_milliseconds(100);
    freeQuadTreeCompress(&tmp);
    messageUser("VIde les images 25/100");
    MLV_wait_milliseconds(100);
    debugMessage(1,"clear quadTree");
    messageUser("On quitte 70/100");
    MLV_wait_milliseconds(100);
    closeImage(&img);
    messageUser("Good Bye 100/100");
    MLV_wait_milliseconds(100);
    closeWindow();
    return 0;
}
