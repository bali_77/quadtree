#include "../header/openImg.h"

/** \brief Demande à l'utilisateur de charger une image , ici les formats pris en charge sont les formats exterieur géré par la
lib MLV et les perso aux projets gmc/qtc
 *
 * \param type int le type d'image 0 / exterieur 1/ gmc qtc
 * \return myImage Return une structure contenant l'image chargé
 *
 */
myImage loadImage(int type){

    FILE * test;
    myImage tmp= { NULL,NULL};
    char *adresse = NULL;
    tmp.TAUXERREUR = DEFAULTERROR;

    if( type == 0){

    /* on demande l'adresse de l'image */
    messageUser("Entrez l'adresse de l'image");
    askUser(&adresse);
    debugMessage(2,"on a eu de l'utilisateur",adresse);
    tmp.img = MLV_load_image(adresse);

        if( tmp.img != NULL){
        debugMessage(1,"Ouverture reussi");
        messageUser("Ouverture réussi ");
        MLV_resize_image(tmp.img,RESX,RESY);
        }
        else{
        debugMessage(1,"Ouverture raté");
        messageUser("Ouverture raté");
        }
    }
    else if ( type == 1){


        /* on demande l'adresse de l'image */
        messageUser("Entrez l'adresse de l'image");
        askUser(&tmp.fimg);
        debugMessage(2,"on a eu de l'utilisateur",adresse);

        if( parser(tmp.fimg,"qtc") != 1 && parser(tmp.fimg,"gmc") != 1){
        debugMessage(1,"Ouverture raté...");
        messageUser("Ouverture raté...");
            return tmp;
        }

        if( parser(tmp.fimg,"qtc") == 1){
            tmp.ext[0]='q';
            tmp.ext[1]='t';
            tmp.ext[2]='c';
            test = fopen(tmp.fimg,"rb+");
        }
        else if( parser(tmp.fimg,"gmc") == 1){
            tmp.ext[0]='g';
            tmp.ext[1]='m';
            tmp.ext[2]='c';
            test = fopen(tmp.fimg,"r+");
        }

        if(test != NULL){
            debugMessage(1,"Ouverture reussi");
            messageUser("Ouverture réussi ");
            MLV_wait_milliseconds(100);

            fclose(test);
        }
        else{
        debugMessage(1,"Ouverture raté");
        messageUser("Ouverture raté");
        }
    }


    return tmp;
}

/** \brief On referme toutes les images ouvertes dans la structure
 *
 * \param img myImage* la structure myImage
 * \return void Return void
 *
 */
void closeImage(myImage * img){

    debugMessage(1,"Fermeture des images");
    if( (*img).img != NULL){
        debugMessage(1,"Fermeture de img mlv");
        MLV_free_image((*img).img);
        (*img).img = NULL;
    }
    if( (*img).fimg != NULL){
        debugMessage(1,"Fermeture de img file");
        free((*img).fimg);
        (*img).fimg = NULL;

    }

}


/** \brief parseur pour les extensions qtc et gmc
 *
 * \param name char* name le nom de l'image + son extension ( img.extension par exemple )
 * \param endd char* endd l'exentension exemple gmc ...
 * \return int 0 ou 1 , 0 erreur 1 ok
 *
 */
int parser(char * name, char * endd){

    int sName = strlen(name);

    int i,j;

    for(i=0; name[i] != '.' && i <= sName; i++);
    if( i >= sName ){
        return 0;
    }
    i++;
    for(j=0; i < sName ; i++,j++){
        if( name[i] != endd[j]){
            return 0;
        }

    }

    if(  j == 2 ){
        return 0;
    }

    return 1;

}
