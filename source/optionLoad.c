#include "../header/optionLoad.h"


/** \brief on traute les options si elles sont valides
 *
 * \param opt int option à traiter
 * \param img myImage* l'image
 * \param vit int* vitesse/erreur
 * \param t QuadTree le quadtree
 * \return void void
 *
 */
void loadOpt(int opt, myImage *img, int *vit, QuadTree t){

    switch(opt){
        case 0:
            closeImage(img);
            (*img) = loadImage(0);
            break;
        case 1:
            closeImage(img);
            (*img) = loadImage(1);
            break;
        case 2:
            saveQuadTree(t);
            break;
        case 3:
            saveQuadTree(t);
            break;
        case 7:
            if( (*vit) < 3000){
            (*vit) = (*vit) + 10;
            char str[15];
            sprintf(str,"%d ++",(*vit));
            messageUser(str);
            }
            break;
        case 8:
            if( (*vit) > 0){
                (*vit) = (*vit) - 10;
                char str[15];
                sprintf(str,"%d --",(*vit));
                messageUser(str);
            }
            break;



    }

}
