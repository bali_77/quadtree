#include "../header/quadTree.h"


/** \brief on traite dej pour savoir quel fichier on a img exterieure ou bien gmc/qtc
 *
 * \param img myImage* notre image à traiter
 * \param vitesse int le taux d'erreur
 * \param a pixel un pixel base 0 0
 * \param b pixel un pixel base MAX X MAX Y
 * \param root QuadTree* notre quadtree
 * \return void
 *
 */
void postTraitement(myImage *img, int vitesse, pixel a, pixel b , QuadTree *root ){

    FILE * tmp;
    if((*img).img != NULL){
        freeQuadTree(&*root);
        *root = initQuadTree();
        traitement(img,vitesse,a,b,root);

    }
    else if( (*img).fimg != NULL ){
        /* on affiche le fichier */
        if( strcmp((*img).ext,"qtc") == 0){
            tmp = fopen((*img).fimg,"rb");

            *root = initQuadTree();

             printf("qtc \n");
            traitementQtc(tmp,vitesse,root);
            fclose((tmp));
        }
        if( strcmp((*img).ext,"gmc") == 0){
            tmp = fopen((*img).fimg,"r");
            *root = initQuadTree();
             rgba t = {0,0,0,0};
             printf("gmc \n");
            traitementGmc(tmp,vitesse,root,&t);
            fclose(tmp);
        }
    }
    else{
        messageUser("Rien à ouvrir...");
        MLV_wait_seconds(1);
    }
}



/** \brief on traite un gmc
 *
 * \param img FILE* l'iamge concernee
 * \param vitesse int la vitesse/taux erreur
 * \param root QuadTree* la sauvegarde du traitement
 * \param oldColor rgba* l'ancienne couleur utilisé
 * \return void
 *
 */
void traitementGmc(FILE *img, int vitesse, QuadTree * root,rgba *oldColor){

    char c;

    if( (c = fgetc(img)) == EOF){
        return;
    }

    while( c ==  ' ' || c == '\n'){
        c = fgetc((img));
    }

    if( c == '0'){
        /* on alloue le noeud et on passe dessus */
        (*root)->NO = getEmptyNode(*oldColor,1);
        (*root)->NE = getEmptyNode(*oldColor,2);
        (*root)->SO = getEmptyNode(*oldColor,3);
        (*root)->SE = getEmptyNode(*oldColor,4);
        traitementGmc(img,vitesse,&(*root)->NO,oldColor);
        traitementGmc(img,vitesse,&(*root)->NE,oldColor);
        traitementGmc(img,vitesse,&(*root)->SO,oldColor);
        traitementGmc(img,vitesse,&(*root)->SE,oldColor);
    }
    else if( c == '1'){
        /* on recupere les 4 * 8 octects */
        char tab[4];
        int i;

        c = fgetc((img));
        while( c ==  ' ' || c == '\n'){
        c = fgetc((img));
        }

        if( c == 'x'){

            (*root)->color.red = (*oldColor).red;
            (*root)->color.blue = (*oldColor).blue;
            (*root)->color.green = (*oldColor).green;
            (*root)->color.alpha = (*oldColor).alpha;
            return;
        }
        tab[0] = c;

        /* red */
        i = 1;
        while( (c = fgetc(img)) != ' ' ){
            tab[i] = c;
            i++;
        }
        tab[i] = '\0';
        (*root)->color.red = atoi(tab);
        /* green*/
        i = 0;
        while( (c = fgetc(img)) != ' ' ){
            tab[i] = c;
            i++;
        }
        tab[i] = '\0';
        (*root)->color.green = atoi(tab);
                /* blue*/
        i = 0;
        while( (c = fgetc(img)) != ' ' ){
            tab[i] = c;
            i++;
        }
        tab[i] = '\0';
        (*root)->color.blue = atoi(tab);
                /* alpha */
        i = 0;
        while( (c = fgetc(img)) != ' ' ){
            tab[i] = c;
            i++;
        }
        tab[i] = '\0';
        (*root)->color.alpha = atoi(tab);

        (*oldColor).red = (*root)->color.red;
            (*oldColor).green = (*root)->color.green;
                (*oldColor).blue = (*root)->color.blue;
                        (*oldColor).alpha = (*root)->color.alpha;

        (*root)->mColor = MLV_convert_rgba_to_color((*root)->color.red,(*root)->color.green,(*root)->color.blue,(*root)->color.alpha);


        return;
    }

}

/** \brief traite une image qtc
 *
 * \param img myImage* l'image
 * \param vitesse int vitesse/tauxerreur
 * \param root QuadTree* le quadTree ou on va l'enregistrer
 * \return void
 *
 */
void traitementQtc(FILE *img, int vitesse, QuadTree * root){

    int c;
    rgba t = {0,0,0,0};

    fread(&c,sizeof(c),1,img);


    if( c == 0){
        /* on alloue le noeud et on passe dessus */
        (*root)->NO = getEmptyNode(t,1);
        (*root)->NE = getEmptyNode(t,2);
        (*root)->SO = getEmptyNode(t,3);
        (*root)->SE = getEmptyNode(t,4);
        traitementQtc(img,vitesse,&(*root)->NO);
        traitementQtc(img,vitesse,&(*root)->NE);
        traitementQtc(img,vitesse,&(*root)->SO);
        traitementQtc(img,vitesse,&(*root)->SE);
    }
    else if( c == 1){
        /* on recupere les 4 * 8 octects */

        fread(&c,sizeof(int),1,img);
        (*root)->color.red = c;
        fread(&c,sizeof(int),1,img);
        (*root)->color.green = c;
        fread(&c,sizeof(int),1,img);
        (*root)->color.blue = c;
        fread(&c,sizeof(int),1,img);
        (*root)->color.alpha = c;
        (*root)->mColor = MLV_convert_rgba_to_color((*root)->color.red,(*root)->color.green,(*root)->color.blue,(*root)->color.alpha);


        return;
    }

}


/** \brief convertie un tableau de 8 bit en int
 *
 * \param tab int* tableau de int
 * \return int un int
 *
 */
int convertBitToInt(int *tab){

    return tab[0]*128 + tab[1]*64 + tab[2]*32 + tab[3]*16 + tab[4]*8 + tab[5]*4 + tab[6]*2 + tab[1]*1;

}

/** \brief on traite une image exterieur
 *
 * \param img myImage* l'image dans la lib mlv
 * \param vitesse int la vitesse de traitement/ erreur
 * \param a pixel pixel 0 0
 * \param b pixel pixel MAX X MAX Y
 * \param root QuadTree* notre quadTRee ou on va enregistrer
 * \return void
 *
 */
void traitement(myImage *img, int vitesse , pixel a, pixel b, QuadTree * root ){

    /* on lance le traitement */
    /* version recursive sans perte */
    /* on a le rectangle a b c d  // a pour haut gauche b haut droite c gauche bas d bas droite*/
    /* faire le traitement */

    /* cas d arret */
    if( (*root) == NULL ){
        debugMessage(2,"STOP ROOT NULL","On passe");
        return;
    }

    /* on recupere la couleur de la zone */
    (*root)->erreur = moyenneZoneRgba2(img,a,b,&(*root)->color);
    (*root)->mColor = MLV_convert_rgba_to_color((*root)->color.red,(*root)->color.green,(*root)->color.blue,(*root)->color.alpha);

    if(((*root)->erreur) <= (*img).TAUXERREUR || areaRectangle(a,b) == 0){
        debugMessage(1,"Taux erreur bonne on stoppe");
        return;
    }

    /* carre haut gauche */
    pixel aa,bb;
    int i;
    for(i=0; i < 4 ; i++){
    aa = a;
    bb = b;
    getRectangle(i,&aa,&bb);

    /* traitment avec la distance avec erreur */
            if( i == 0){
                (*root)->NO = getEmptyNode((*root)->color, 1);
                traitement(img,1,aa,bb,&(*root)->NO);
            }
            else if( i == 1){
                (*root)->NE = getEmptyNode((*root)->color, 2);
                traitement(img,1,aa,bb,&(*root)->NE);
            }
            else if( i == 2){
                (*root)->SO = getEmptyNode((*root)->color, 3);
                traitement(img,1,aa,bb,&(*root)->SO);
            }
            else if( i == 3){
                (*root)->SE = getEmptyNode((*root)->color, 4);
                traitement(img,1,aa,bb,&(*root)->SE);
            }


    /* fin */
    }



}

/** \brief renvoie l'air d'un rectangle
 *
 * \param a pixel
 * \param b pixel
 * \return int aire du rectangle
 *
 */
int areaRectangle(pixel a , pixel b){

    int tmp = 0;

    tmp = (b.x - a.x ) * ( b.y - a.y);

    return tmp <= 1?0:1;

}

/** \brief divise un rectangle en 4 puis renvoie les coordonnes haut gauche dans a et bas droite dans b de la position souhaite
 *
 * \param number int 1 NO, 2NE, 3SO ou 4SE
 * \param a pixel* haut gauche
 * \param b pixel* bas droite
 * \return void
 *
 */
void getRectangle(int number , pixel *a , pixel *b){

    int d1 = (*b).x - (*a).x;
    int d2 = (*b).y - (*a).y;
    switch(number){
        case 0:
                (*b).x = (*b).x - (d1/2);
                (*b).y = (*b).y - (d2/2);
              /*  (*b).x = (*a).x + (d1/2);
                (*b).y = (*a).y + (d2/2);*/
            break;
        case 1:
                (*a).x = (*a).x + (d1/2);
                (*b).y = (*b).y - (d2/2);
            break;
        case 2:
                (*a).y = (*a).y + (d2/2); /* error 1 */
                (*b).x = (*b).x - (d1/2);
            break;
        case 3:
                (*a).x = (*a).x + (d1/2);
                (*a).y = (*a).y + (d2/2);
            break;
        default:
            debugMessage(1,"Erreur valeur entre pour le rectangle");
    }


}


/** \brief renvoie un quadtree vide
 *
 * \return QuadTree
 *
 */
QuadTree initQuadTree(){

    debugMessage(1,"Init quatree avec une couleur par defaut");

    QuadTree tmp = (Noeud*)malloc(sizeof(Noeud));

    if( (tmp) == NULL ){
        debugMessage(1,"Erreur allocation quadTree");
        exit(EXIT_FAILURE);
    }


    (tmp)->lenght = 0;
    (tmp)->freeKey = 0;
    (tmp)->NE = NULL;
    (tmp)->NO = NULL;
    (tmp)->SE = NULL;
    (tmp)->SO = NULL;
    (tmp)->color.alpha = 0;
    (tmp)->color.blue = 0;
    (tmp)->color.green = 0;
    (tmp)->color.red = 0;
    (tmp)->mColor = MLV_COLOR_BLACK;
    (tmp)->erreur = 0;

    return tmp;
}

/** \brief renvoie un noeud vide avec une couleur predefinie et sa pos dans un carre
 *
 * \param rgba rgba la couleur
 * \param i int sa position 1 , 2 , 3 ou 4
 * \return QuadTree
 *
 */
QuadTree getEmptyNode(rgba rgba, int i){

    Noeud * tmp = NULL;
    tmp = initQuadTree();

    (tmp)->color.alpha = rgba.alpha;
    (tmp)->color.blue = rgba.blue;
    (tmp)->color.green = rgba.green;
    (tmp)->color.red = rgba.red;
    (tmp)->mColor = MLV_convert_rgba_to_color(rgba.red,rgba.green,rgba.blue,rgba.alpha);
    (tmp)->lenght = i ;

    return tmp;
}

/** \brief renvoie si oui ou non c'est une feuille
 *
 * \param tmp QuadTree*
 * \return int 0 non  ou 1 oui
 *
 */
int isLeaf(QuadTree * tmp){

    if( (*tmp)-> NO == NULL){
        if( (*tmp)-> NE == NULL){
            if( (*tmp)-> SO == NULL){
                if( (*tmp)-> SE == NULL){
                    return 1;
                }
            }
        }

    }


    return 0;
}

/** \brief vide un quadtree compréssé
 *
 * \param tmp QuadTree*
 * \return void
 *
 */
void freeQuadTreeCompress(QuadTree * tmp){

    if( *tmp == NULL){
        return;
    }


    if( *tmp != NULL && (*tmp)->freeKey == 0){
        (*tmp)->freeKey = 1;
        freeQuadTreeCompress(&(*tmp)->NO);
    }
    if( *tmp != NULL && (*tmp)->freeKey == 0){
        (*tmp)->freeKey = 1;
        freeQuadTreeCompress(&(*tmp)->NE);
    }
    if( *tmp != NULL && (*tmp)->freeKey == 0){
        (*tmp)->freeKey = 1;
        freeQuadTreeCompress(&(*tmp)->SO);
    }
    if( *tmp != NULL && (*tmp)->freeKey == 0){
        (*tmp)->freeKey = 1;
        freeQuadTreeCompress(&(*tmp)->SE);
    }


    if( *tmp == NULL){
        return;
    }

    free(*tmp);
    *tmp = NULL;

}


/** \brief vide un quadtree
 *
 * \param tmp QuadTree* le quadTree
 * \return void
 *
 */
void freeQuadTree(QuadTree *tmp){

    if( *tmp == NULL){
        return;
    }

    freeQuadTree(&(*tmp)->NO);
    freeQuadTree(&(*tmp)->NE);
    freeQuadTree(&(*tmp)->SO);
    freeQuadTree(&(*tmp)->SE);

    if( *tmp == NULL){
            /* cas double free */
        return;
    }

    free(*tmp);
    *tmp = NULL;
}
