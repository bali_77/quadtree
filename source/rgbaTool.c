#include "../header/rgbaTool.h"

/** \brief renvoie moyenne d'une zone de l'image en mlv
 *
 * \param img myImage* l'image
 * \param a pixel haut gauche
 * \param b pixel bas droite
 * \return MLV_Color la couleur en mlv
 *
 */
MLV_Color moyenneZone(myImage *img, pixel a , pixel b){

    /* MLV_Color MLV_convert_rgba_to_color(Uint8 red, Uint8 green, Uint8 blue, Uinit8 alpha) */
    rgba tmp;
    rgba **tab = getRgbaTab(b.x-a.x,b.y-a.y);
    int i,j,k,l;
    int size=0;
    if( (*img).img != NULL){
            for(i=a.x,k=0; i < b.x ; i++,k++)
                for(j=a.y,l=0; j < b.y ; j++,l++){
                    MLV_get_pixel_on_image((*img).img,i,j,&tab[k][l].red,&tab[k][l].green,&tab[k][l].blue,&tab[k][l].alpha);
                    size++;
                }

    }

    tmp = moyenne(tab,b.x-a.x,b.y-a.y);
    return MLV_convert_rgba_to_color(tmp.red, tmp.green, tmp.blue, tmp.alpha);

}

/** \brief moyenne zone d'une image avec taux erreur calculé en temps réel
 *
 * \param img myImage* l'image
 * \param a pixel haut gauche
 * \param b pixel bas droite
 * \param color rgba* couleur moyenne
 * \return double taux erreur
 *
 */
double moyenneZoneRgba2(myImage * img, pixel a, pixel b, rgba * color){

    int i,j;
    int size = 0;
    double erreur = 0;
    rgba col = {0,0,0,0};
    double rr = 0 , gg=0, bb= 0, aa= 0;


        if( (*img).img != NULL){
            for(i=a.x; i < b.x ; i++)
                for(j=a.y; j < b.y ; j++){
                    MLV_get_pixel_on_image((*img).img,i,j,&col.red,&col.green,&col.blue,&col.alpha);
                    rr += col.red;
                    gg += col.green;
                    bb += col.blue;
                    aa += col.alpha;
                    size++;
                }

        }


        (*color).red = rr/size;
        (*color).green= gg/size;
        (*color).blue = bb/size;
        (*color).alpha = aa/size;

            for(i=a.x; i < b.x ; i++)
                for(j=a.y; j < b.y ; j++){
                    MLV_get_pixel_on_image((*img).img,i,j,&col.red,&col.green,&col.blue,&col.alpha);
                    erreur += distance((*color),col);
                }


        return erreur;

}

/** \brief anciene version voir moyenneZoneRgba2
 *
 * \param img myImage*
 * \param a pixel
 * \param bn pixel
 * \param color rgba*
 * \return double
 *
 */
double moyenneZoneRgba(myImage *img, pixel a, pixel b, rgba *color){
    rgba **tab = getRgbaTab(b.x-a.x,b.y-a.y);
    int i,j,k,l;
    double erreur = 0;

    if( (*img).img != NULL){
            for(i=a.x,k=0; i < b.x ; i++,k++)
                for(j=a.y,l=0; j < b.y ; j++,l++){
                    MLV_get_pixel_on_image((*img).img,i,j,&tab[k][l].red,&tab[k][l].green,&tab[k][l].blue,&tab[k][l].alpha);
                }

    }


    (*color) = moyenne(tab,b.x-a.x,b.y-a.y);

        for(i=a.x,k=0; i < b.x ; i++,k++)
            for(j=a.y,l=0; j < b.y ; j++,l++){
                    erreur += distance((*color),tab[k][l]);
                }




    freeTab2d(tab,b.x-a.x);
    return erreur;
}




/** \brief on libere un tableau 2d rgba
 *
 * \param tab rgba**
 * \param width int
 * \return void
 *
 */
void freeTab2d(rgba **tab, int width){

    int i;

    for(i=0; i < width; i++){
        free(tab[i]);
        tab[i]=NULL;
    }

    free(tab);

}

/** \brief renvoie un tableau 2d rgba initialisé
 *
 * \param width int width
 * \param height int height
 * \return rgba**
 *
 */
rgba ** getRgbaTab(int width, int height){

    rgba ** tmp = NULL;

    tmp = (rgba**)malloc(sizeof(rgba*)*width);

    if( tmp == NULL){
        debugMessage(1,"erreur alloc tab2d rgba fatal error");
        exit(EXIT_FAILURE);
    }

    int i;

    for(i=0; i < width; i++){
            tmp[i] = (rgba*)malloc(sizeof(rgba)*height);
            if( tmp[i] == NULL){
                        debugMessage(1,"erreur alloc tab2d partie height rgba fatal error");
                        exit(EXIT_FAILURE);
            }
        }

            return tmp;

}


/** \brief distance entre deux couleurs
 *
 * \param un rgba color un
 * \param deux rgba color deux
 * \return double resultat
 *
 */
double distance(rgba un , rgba deux){
    /*debugMessage(1,"Calcul de la distance");*/
    return sqrt((un.red-deux.red)*(un.red-deux.red)
                +(un.green-deux.green)*(un.green-deux.green)
                +(un.blue-deux.blue)*(un.blue-deux.blue)
                +(un.alpha-deux.alpha)*(un.alpha-deux.alpha));
}

/** \brief renvoie la moyenne des couleurs rgba du tableau
 *
 * \param tab rgba** le tableau rgba
 * \param width int sa taille x
 * \param height int sa taille y
 * \return rgba une couleur moyenne
 *
 */
rgba moyenne(rgba **tab,int width, int height){

    debugMessage(1,"Calcul de la moyenne");
    rgba z = {0,0,0,0};
    double red = 0 , green = 0, blue = 0 , alpha =0;
    int i = 0, j =0;

    for(i=0;i < width; i++){
        for(j=0; j < height ; j++){
        red += tab[i][j].red;
        green += tab[i][j].green;
        blue += tab[i][j].blue;
        alpha += tab[i][j].alpha;
        }
    }

    z.red = red / (width*height);
    z.green = green / (width*height);
    z.blue = blue / (width*height);
    z.alpha = alpha / (width*height);

    return z;


}

/** \brief renvoie le taux d'erreur entre la couleur moyenne et la couleur reelle
 *
 * \param img myImage* l'image
 * \param a pixel zone haut gauche
 * \param b pixel zone bas droite
 * \param z rgba la couleur moyenne
 * \return double resulat
 *
 */
double erreurZ(myImage *img,pixel a, pixel b, rgba z){

    debugMessage(1,"Calcul de l'erreur z");
    double erreur = 0;
    int i = 0, j = 0 , k = 0 , l = 0;
    rgba tmp;

    if( (*img).img != NULL){
            for(i=a.x,k=0; i < b.x ; i++,k++)
                for(j=a.y,l=0; j < b.y ; j++,l++){
                    MLV_get_pixel_on_image((*img).img,i,j,&tmp.red,&tmp.green,&tmp.blue,&tmp.alpha);
                    erreur += distance(tmp,z);
                }

    }

    if( DEBUG){
        fprintf(stdout,"%f erreur \n",erreur);
    }

    return erreur;
}
