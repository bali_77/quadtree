#include "../header/save.h"


/** \brief ecrit une couleur en controlant que c'est pas un doublon
 *
 * \param file FILE* le fichier
 * \param root QuadTree le quadtree
 * \param carre int la position du carre
 * \return void
 *
 */
void writeColor(FILE *file, QuadTree root, int carre){

            char str[10];

            if( root->lenght == carre){
            char str[10];

            sprintf(str,"%d ",root->color.red);
            fputs(str,file);
            sprintf(str,"%d ",root->color.green);
            fputs(str,file);
            sprintf(str,"%d ",root->color.blue);
            fputs(str,file);
            sprintf(str,"%d ",root->color.alpha);
            fputs(str,file);

            }
            else{
                sprintf(str,"%d ",root->lenght);
                fputc('x',file);
                fputc(' ',file);
            }

}

/** \brief ecrit dans file le quadtree compresse/non compresse en mode ASCII
 *
 * \param file FILE* le fichier de sauvegare
 * \param root QuadTree le fichier
 * \param color int
 * \return void
 *
 */
void writeTreeCompress2(FILE *file, QuadTree root, int color){


    if( root->NE == NULL && root->NO == NULL && root->SE == NULL && root->SO == NULL){
            fputc('1',file); /* on a une feuille */
            fputc(' ',file);
            writeColor(file,root,color);
            fputc('\n',file);
            return;
    }

    fputc('0',file);
    fputc('\n',file);
    writeTreeCompress2(file,root->NO,1);
    writeTreeCompress2(file,root->NE,2);
    writeTreeCompress2(file,root->SO,3);
    writeTreeCompress2(file,root->SE,4);

}

/** \brief voir writeTreeCompress2
 *
 * \param file FILE*
 * \param root QuadTree
 * \return void
 *
 */
void writeTreeCompress(FILE * file ,QuadTree root){

    /* on parcours l'arbre et on ecrit dedans */
    if( (root)-> NO == NULL && (root)->NE == NULL && (root)->SE == NULL && (root)->SO == NULL){
            fputc('1',file);
            fputc(' ',file);
            char str[10];

            sprintf(str,"%d ",root->color.red);
            str[4]='\0';
            fputs(str,file);
            sprintf(str,"%d  ",root->color.green);
            str[4]='\0';
            fputs(str,file);
            sprintf(str,"%d ",root->color.blue);
            str[4]='\0';
            fputs(str,file);
            sprintf(str,"%d ",root->color.alpha);
            str[4]='\0';
            fputs(str,file);
            fputc('\n',file);
            return;
    }


    char c[5];
    fputc('0',file);/* ajoute un noeud */
    fputc('\n',file);

    if( root->NO->lenght == 0){
    writeTreeCompress(file,root->NO);
    }
    else{
        fputc('\n',file);
        sprintf(c,"%d\n",root->NO->lenght);
        fputc(c[0],file);
        fputc(c[1],file);
        return;
    }

    if( root->NE->lenght == 1){
    writeTreeCompress(file,root->NE);
    }
    else{
        fputc('\n',file);
        sprintf(c,"%d\n",root->NE->lenght);
        fputc(c[0],file);
        fputc(c[1],file);
        return;
    }

    if( root->SO->lenght == 2){
    writeTreeCompress(file,root->SO);
    }
    else{
                    fputc('\n',file);
        sprintf(c,"%d\n",root->SO->lenght);
        fputc(c[0],file);
        fputc(c[1],file);
        return;
    }

    if( root->SE->lenght == 3){
    writeTreeCompress(file,root->SE);
    }
    else{
                    fputc('\n',file);
        sprintf(c,"%d\n",root->SE->lenght);
        fputc(c[0],file);
        fputc(c[1],file);
        return;
    }


}

/** \brief sauvegarde un quadtree non minimisé
 *
 * \param root QuadTree le quadtree
 * \return void
 *
 */
void saveQuadTree(QuadTree root){

    char * sFile = NULL;

    askUser(&sFile);

    if( parserSave(sFile,"qtc") == 0 && parserSave(sFile,"gmc") == 0){
        messageUser("Erreur nom fichier");
        free(sFile);
        return;
    }


    FILE * file;

    if( parserSave(sFile,"qtc") == 1){
     file = fopen(sFile,"wb+");

    if( file == NULL){
        messageUser("Erreur ouverture fichier");
        free(sFile);
        return;
    }

    writeTreeBinary(file,root);
    }
    else if(parserSave(sFile,"gmc") == 1){
     file = fopen(sFile,"w+");

    if( file == NULL){
        messageUser("Erreur ouverture fichier");
        free(sFile);
        return;
    }

    writeTreeCompress2(file,root,1);
    }

    /*fputc(EOF,file);*/

    messageUser("Fichier bien enregistré");
    fclose(file);
    free(sFile);
}


/** \brief ecrit dans file en mode binaire , compresse ou non
 *
 * \param file FILE* ou on sauvegarde
 * \param root QuadTree ce que l'on sauvegarde
 * \return void
 *
 */
void writeTreeBinary(FILE* file, QuadTree root){

    int zero = 0;
    int un = 1;

    if( root == NULL){
        return;
    }

    /* on parcours l'arbre et on ecrit dedans */
    if( (root)-> NO == NULL && (root)->NE == NULL && (root)->SE == NULL && (root)->SO == NULL){
            /* on met un 1 puis le code couleur */
            fwrite(&un,sizeof(int),1,file);
                fwrite(&(root)->color.red,sizeof(int),1,file);
                        fwrite(&(root)->color.green,sizeof(int),1,file);
                                    fwrite(&(root)->color.blue,sizeof(int),1,file);
                                                fwrite(&(root)->color.alpha,sizeof(int),1,file);



            return;
    }

    fwrite(&zero,sizeof(int),1,file);
    /*fputc('0',file);*//* ajoute un noeud */
    writeTreeBinary(file,root->NO);
    writeTreeBinary(file,root->NE);
    writeTreeBinary(file,root->SO);
    writeTreeBinary(file,root->SE);

}


/** \brief on verifie que loe nom donné par l'utilisateur est bon
 *
 * \param name char* nom
 * \param endd char* extension à verifier
 * \return int
 *
 */
int parserSave(char * name, char * endd){

    int sName = strlen(name);

    int i,j;

    for(i=0; name[i] != '.' && i <= sName; i++);
    if( i >= sName ){
        return 0;
    }
    i++;
    for(j=0; i < sName ; i++,j++){
        if( name[i] != endd[j]){
            return 0;
        }

    }

    if(  j == 2 ){
        return 0;
    }

    return 1;

}

