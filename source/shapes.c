#include "../header/shapes.h"


/** \brief dessine un rectangle plein grace à la couleur rgba sur la fenetre mlv
 *
 * \param a pixel haut gauche
 * \param b pixel bas droite
 * \param color rgba couleur
 * \return void
 *
 */
void drawFilledRectangle(pixel a , pixel b , rgba color){

    MLV_Color tmp = MLV_convert_rgba_to_color(color.red, color.green , color.blue, color.alpha);


    /*MLV_draw_filled_rectangle(a.x,a.y + SIZEM,b.x-a.x,b.y-a.y,MLV_COLOR_BLACK);*/
    MLV_draw_filled_rectangle(a.x,a.y + SIZEM,b.x-a.x,b.y-a.y,tmp);
    MLV_actualise_window();


}


/** \brief dessine un rectangle plein grace à la couleur mlv sur la fenetre mlv
 *
 * \param a pixel haut gauche
 * \param b pixel bas droite
 * \param color MLV_Color couleur
 * \return void
 *
 */
void drawFilledRectangleM(pixel a, pixel b, MLV_Color color){

    MLV_draw_filled_rectangle(a.x,a.y + SIZEM,b.x-a.x,b.y-a.y,color);
    MLV_actualise_window();
}
