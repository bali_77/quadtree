#include "../header/treeMlv.h"


/** \brief dessine le quadTree sur l'entree mlv
 *
 * \param t QuadTree* le quadtree à dessiner
 * \param a pixel 0,0
 * \param b pixel MAXS,MAXY
 * \return void
 *
 */
void printTree(QuadTree *t, pixel a, pixel b){


    if( (*t)-> NO == NULL && (*t)->NE == NULL && (*t)->SE == NULL && (*t)->SO == NULL){
        drawFilledRectangleM(a,b,(*t)->mColor);
        return;
    }
    /* carre haut gauche */
    pixel aa,bb;
    int i;
    for(i=0; i < 4 ; i++){
    aa = a;
    bb = b;
    getRectangle(i,&aa,&bb);

    /* traitement avec la distance avec erreur */
            if( i == 0){
                printTree(&(*t)->NO,aa,bb);
            }
            else if( i == 1){
                printTree(&(*t)->NE,aa,bb);
            }
            else if( i == 2){
                printTree(&(*t)->SO,aa,bb);
            }
            else if( i == 3){
                printTree(&(*t)->SE,aa,bb);
            }


    /* fin */
    }


}
