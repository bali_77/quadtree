#include "../header/windowMlv.h"

/** \brief lance une fenetre mlv
 *
 * \return void
 *
 */
void launchWindow(){
    debugMessage(1,"Lancement de la fenetre");
    MLV_create_window("Projet S5 PROGC", "Projet S5 PROGC", RESX, RESYM);
    barMenu();
}


/** \brief on affiche la barre des menus
 *
 * \return void
 *
 */
void barMenu(){
        debugMessage(1,"Creation de la barre de menu");
        MLV_draw_filled_rectangle(0,0,RESX,SIZEM, MLV_COLOR_WHITE);
        MLV_draw_text(LOAD*SIZEI,0,LOADS,MLV_COLOR_BLACK);
        MLV_draw_text(LOADU*SIZEI,0,LOADUS,MLV_COLOR_BLACK);
        MLV_draw_text(SAVEB*SIZEI,0,SAVEBS,MLV_COLOR_BLACK);
        MLV_draw_text(SAVEM*SIZEI,0,SAVEMS,MLV_COLOR_BLACK);
        MLV_draw_text(QUIT*SIZEI,0,QUITS,MLV_COLOR_BLACK);
        MLV_draw_text(PLUS*SIZEI,0,PLUSS,MLV_COLOR_BLACK);
        MLV_draw_text(MOINS*SIZEI,0,MOINSS,MLV_COLOR_BLACK);
        MLV_draw_text(START*SIZEI,0,STARTS,MLV_COLOR_BLACK);
        MLV_actualise_window();

}

/** \brief ferme la fentre mlv
 *
 * \return void
 *
 */
void closeWindow(){
    MLV_wait_seconds(3);
    messageUser("Fermeture en cours");
    debugMessage(1,"Fermeture de la fenetre");
    MLV_free_window();
}


/** \brief envoie un message à l'utilisateur sur la bersion graphique
 *
 * \param s char* le message
 * \return void
 *
 */
void messageUser(char *s){

    debugMessage(2,"Affichage d'un message pour l'utilisateur",s);
    MLV_draw_filled_rectangle(MESSAGE*SIZEI,0,RESX-(MESSAGE*SIZEI),SIZEM,MLV_COLOR_WHITE);
    MLV_draw_text(MESSAGE*SIZEI,0,s,MLV_COLOR_BLACK);
    MLV_actualise_window();
}

/** \brief Demande une chaine de caractere a l'utilisateur
 *
 * \param s char** le message
 * \return void
 *
 */
void askUser(char **s){
    debugMessage(1,"On demande un message à l'utilisateur");
    MLV_wait_input_box(MESSAGE*SIZEI,0,RESX-(MESSAGE*SIZEI),SIZEM, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_COLOR_WHITE,":",s);
    debugMessage(1,"Fin de la demande");
}

/** \brief on traite les evenements en renvoyant le code de l'option sinon -2
 *
 * \param e myEvent* notre evenement
 * \return int l'option
 *
 */
int loadEvent(myEvent *e){
    /*debugMessage(1,"On rentre dans loadevent");*/
/* -1 si pas de clic */
    int sizei = SIZEI;

        if( (*e).y < SIZEM){
        int menu = (*e).x/sizei;
            switch(menu){
                case 0:
                    if( (*e).oldevent != 0){
                    barMenu();
                    MLV_draw_filled_rectangle(((*e).x/sizei)*sizei,0,sizei,SIZEM,MLV_COLOR_BLACK);
                    MLV_draw_text(LOAD*SIZEI,0,LOADS,MLV_COLOR_WHITE);
                    MLV_actualise_window();
                    (*e).oldevent = 0;
                    }
                    break;
                case 1:
                    if( (*e).oldevent != 1){
                    barMenu();
                    MLV_draw_filled_rectangle(((*e).x/sizei)*sizei,0,sizei,SIZEM,MLV_COLOR_BLACK);
                    MLV_draw_text(LOADU*SIZEI,0,LOADUS,MLV_COLOR_WHITE);
                    MLV_actualise_window();
                    (*e).oldevent = 1;
                    }
                    break;
                 case 2 :
                    if( (*e).oldevent != 2){
                    barMenu();
                    MLV_draw_filled_rectangle(((*e).x/sizei)*sizei,0,sizei,SIZEM,MLV_COLOR_BLACK);
                    MLV_draw_text(SAVEB*SIZEI,0,SAVEBS,MLV_COLOR_WHITE);
                    MLV_actualise_window();
                    (*e).oldevent = 2;
                    }
                    break;
                 case 3 :
                    if( (*e).oldevent != 3){
                    barMenu();
                    MLV_draw_filled_rectangle(((*e).x/sizei)*sizei,0,sizei,SIZEM,MLV_COLOR_BLACK);
                    MLV_draw_text(SAVEM*SIZEI,0,SAVEMS,MLV_COLOR_WHITE);
                    MLV_actualise_window();
                    (*e).oldevent = 3;
                    }
                    break;
                  case 6 :
                    if( (*e).oldevent != 6){
                    barMenu();
                    MLV_draw_filled_rectangle(((*e).x/sizei)*sizei,0,sizei,SIZEM,MLV_COLOR_BLACK);
                    MLV_draw_text(START*SIZEI,0,STARTS,MLV_COLOR_WHITE);
                    MLV_actualise_window();
                    (*e).oldevent = 6;
                    }
                    break;
                 case 7 :
                    if( (*e).oldevent != 7){
                    barMenu();
                    MLV_draw_filled_rectangle(((*e).x/sizei)*sizei,0,sizei,SIZEM,MLV_COLOR_BLACK);
                    MLV_draw_text(MOINS*SIZEI,0,MOINSS,MLV_COLOR_WHITE);
                    MLV_actualise_window();
                    (*e).oldevent = 7;
                    }
                    break;
                 case 8 :
                    if( (*e).oldevent != 8){
                    barMenu();
                    MLV_draw_filled_rectangle(((*e).x/sizei)*sizei,0,sizei,SIZEM,MLV_COLOR_BLACK);
                    MLV_draw_text(PLUS*SIZEI,0,PLUSS,MLV_COLOR_WHITE);
                    MLV_actualise_window();
                    (*e).oldevent = 8;
                    }
                    break;
                 case 9:
                    if( (*e).oldevent != 9){
                    barMenu();
                    MLV_draw_filled_rectangle(((*e).x/sizei)*sizei,0,sizei,SIZEM,MLV_COLOR_BLACK);
                    MLV_draw_text(QUIT*SIZEI,0,QUITS,MLV_COLOR_WHITE);
                    MLV_actualise_window();
                    (*e).oldevent = 9;
                    }
                    break;
                 default:
                    return -1;
            }
        }


    if( (*e).pressed == 1 && (*e).x > 0 && (*e).x < RESX && (*e).y < SIZEM){
        (*e).pressed = 0;
        return (*e).x / sizei;
    }

    return -1;
}
